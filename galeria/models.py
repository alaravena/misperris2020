from django.db import models

# Create your models here.
class Galeria(models.Model):
    
    imagen = models.ImageField(null=False,upload_to = 'img-gal/')
    nombre_mascota = models.CharField(max_length=200, null=False)
    comentario_breve = models.TextField(null=False)

    def __str__(self):
        return self.nombre_mascota