from rest_framework import serializers
#Modelos que se usan
from rescatados.models import Rescatado
from socios.models import Socio

class RescatadosListaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rescatado
        fields = ['fotografia', 'nombre', 'raza_predominante', 'descripcion', 'estado']

class SociosListaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Socio
        fields = '__all__'
