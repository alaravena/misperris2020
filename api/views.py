from rest_framework import viewsets, permissions
from .serializers import RescatadosListaSerializer, SociosListaSerializer

#Modelos que necesito llamar
from rescatados.models import Rescatado
from socios.models import Socio

# Create your views here.

class RescatadosListaJson(viewsets.ModelViewSet):
    """
    API endpoint que lista los animales rescatados.
    """

    queryset = Rescatado.objects.all().order_by('nombre')
    serializer_class = RescatadosListaSerializer
    #permission_classes = [permissions.IsAuthenticated]

class SociosListaJson(viewsets.ModelViewSet):
    """
    API endpoint que lista los socios de la organización.
    """
    queryset = Socio.objects.all().order_by('email')
    serializer_class = SociosListaSerializer
    #permisson_classes = [permissions.IsAuthenticated]
