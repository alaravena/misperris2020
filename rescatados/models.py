from django.db import models
from django.utils import timezone

#Opciones selector estado rescatado
ESTADO_RESCATADO_CHOICES = (
    ('recatado','Rescatado'),
    ('disponible', 'Disponible'),
    ('adoptado','Adoptado'),
)

# Create your models here.
class Rescatado(models.Model):
    
    fotografia = models.ImageField(null=False,upload_to = 'rescatados/', default = 'rescatados/sin-imagen.jpg')
    nombre = models.CharField(max_length=200, null=False)
    raza_predominante = models.CharField(max_length=50, null=False)
    descripcion = models.TextField(null=False)
    estado = models.CharField(max_length=15, choices=ESTADO_RESCATADO_CHOICES, default='rescatado')

    def __str__(self):
        return self.nombre
    objects = models.Manager()

