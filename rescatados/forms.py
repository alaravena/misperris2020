from django import forms
from .models import Rescatado

class RescatadoAdd(forms.ModelForm):

    class Meta:
        model = Rescatado
        fields = ('fotografia', 'nombre','raza_predominante', 'descripcion', 'estado')
