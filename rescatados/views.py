from django.shortcuts import render, redirect, get_object_or_404
from .forms import RescatadoAdd
from .models import Rescatado

# Create your views here.

def rescatado_add(request):
    if request.method == "POST":
        form = RescatadoAdd(request.POST, request.FILES)
        if form.is_valid():
            rescatado = form.save(commit=False)
            rescatado.save()
            return redirect('rescatado_detail', pk=rescatado.pk)
    else:
        form = RescatadoAdd()
    return render(request, 'rescatados/rescatado_add.html', {'form': form})

def rescatado_list(request):
    rescatados = Rescatado.objects.all()
    return render(request, 'rescatados/rescatado_list.html', {'rescatados': rescatados})

def rescatado_detail(request, pk):
    rescatado = get_object_or_404(Rescatado, pk=pk)
    return render(request, 'rescatados/rescatado_detail.html', {'rescatado': rescatado})

def rescatado_edit(request, pk):
    rescatado = get_object_or_404(Rescatado, pk=pk)
    if request.method == "POST":
        form = RescatadoAdd(request.POST, instance=rescatado)
        if form.is_valid():
            rescatado = form.save(commit=False)
            rescatado.save()
            return redirect('rescatado_list')
    else:
        form = RescatadoAdd(instance=rescatado)
    return render(request, 'rescatados/rescatado_edit.html', {'form': form})

def rescatado_delete(request, pk):  
    rescatado = Rescatado.objects.get(pk=pk)  
    rescatado.delete()  
    return redirect('rescatado_list')  