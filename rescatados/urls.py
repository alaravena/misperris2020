from django.urls import path
from . import views

urlpatterns = [
    path('', views.rescatado_list, name='rescatado_list'),
    path('<int:pk>/', views.rescatado_detail, name='rescatado_detail'),
    path('add', views.rescatado_add, name='rescatado_add'),
    path('<int:pk>/edit/', views.rescatado_edit, name='rescatado_edit'),
    path('<int:pk>/delete/', views.rescatado_delete, name='rescatado_delete'),
]