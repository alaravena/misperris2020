from django.test import TestCase

# Create your tests here.
from django.urls import reverse, resolve
from .models import Rescatado

class TestUrls:

    def test_detail_url(self):
        path = reverse('rescatado_detail', kwargs={'pk': 1})
        assert resolve(path).view_name == 'rescatado_detail'