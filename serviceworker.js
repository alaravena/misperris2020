var staticCacheName = "mis-perris-v" + new Date().getTime();
var filesToCache = [
    '/',
    '/offline',
    '/static/css/estilos.css',
    '/static/imagenes/logo.png',
    '/static/librerias/fontawesome/all.min.css',
];
// Cache on install
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("mis-perris-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        fetch(event.request).then((result)=>{
            return caches.open(staticCacheName).then(function(c) {
                c.put(event.request.url, result.clone())
                return result;
            })
        }).catch(function(e){
            return caches.match(event.request)
        })
    )
});