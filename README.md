# MisPerris2020

Proyecto de clase DWY4101, segundo semestre 2020 DUOC UC
<http://alaravena.pythonanywhere.com/>

> NOTA: Aqui no se aborda virtual enviroments pero es fuertemente aconsejado usarlos.

## Generales

1) Configurar *settings.py*: zona horaria, idioma, host, ruta estática
2) Correr `python manage.py migrate` para sacar las alertas.
3) Crear superusuario `python manage.py createsuperuser`

### 4) Aplicaciones

Agregar aplicación (módulo), en este caso **socios**

`python manage.py startapp socios`

y agregar a *settings.py*, sección intalled apps: **'socios.apps.SociosConfig'**

### 5) Modelo (BD)

Definir el modelo en *models.py* dentro de la carpeta socios
(tipos de campos: <https://docs.djangoproject.com/en/3.1/ref/models/fields/>)

Ojo con los tipos de campos para hacer un select

Se debe agregar el modelo a la base de datos con `python manage.py makemigrations socios` y luego: `python manage.py migrate socios`

### 6) Administración

Dentro de carpeta socios, agregar el modelo al administrador de django en archivo *admin.py* primro importando **from .models import Socio** y luego registrando el modelo **admin.site.register(Socio)**

>La aplicación y modelo podría haberse llamado "Usuarios" o similar. Usé "socios" por fines didácticos.
>Para la actividad siguiente, de mascotas rescatadas, se deben repetir los pasos 4, 5 y 6, poniendo atención en los tipos de campos

### 7) Subida de archivos

Agregar **MEDIA_ROOT** y **MEDIA_URL** en el archivo general de *settings.py*

Como son archivos de imágenes, se debe installar `pip install pillow`

Para visualizar las imágenes cuando se está desarrollando, se debe agregar a *urls.py* al final del archivo:

```python
from django.conf import settings
from django.conf.urls.static import static

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```

Información completa: <https://simpleisbetterthancomplex.com/tutorial/2016/08/01/how-to-upload-files-with-django.html>

### 8) Parar a revisar errores y detalles

En "Socios" había 2 errores: primero, no se listaba en el formulario de admin. Esto se debe a que CharField debe tener definido un numero máximo de caracteres y no lo estaba en el campo "nombre". Además, este campo es usado para ser mostrado en la lista de admin, por lo que no puede ser nullo y por defecto esta habilitado para serlo.

En "destacados" faltaba la función `ef __str__(self):`, que es obligatoria que nos permite listar las entradas, por lo que no estaba agregando nuevas entradas.

### 9) El archivo requirements.txt

Para subir el proyecto a pythonanywere, heroku o cualquier nube, se deben instalar las mismas dependencias que usan en su máquina local y evitar errores de ambiente.
Para esto django usa el archivo **requirements.txt** y la mejor forma de hacerlo es generarlo con `pip freeze > requirements.txt`
Si bajan un repositorio django (como el mio), deben ejecutar `pip install -r requirements.txt` para que se instalen las dependencias.

Mas detalles: <https://medium.com/@hugo.roca/python-requirements-txt-6e6849710e38>

### 10) Archivos estáticos

Antes de comenzar con las vistas, debemor organizar el sitio.
En unidad anterior tenemos archivos que no cambian su contenido como el css, imágenes propias del diseño (no del contenido) como el logo y posiblemente librerías externas. Si hay js que se aplica a todo el sitio, también van aqui.

Debemos confirmar que en *settings.py* tenga definido **STATIC_ROOT**. Este es el directorio de archivos estáticos para cada app de django (por ejemplo un js, css o imagen específica a una vista)

Para archivos comunes a todo el sitio, agregamos a *settings.py* :

```python
STATICFILES_DIRS = (
    BASE_DIR / "common_static",
)
```

Luego creamos una carpeta raiz con el nombre que hemos definido 'common_static'. Dentro de esta carpeta copiamos nuestro contenido estático

### 11) Agregando vistas

#### urls

Para que no se desordene el archivo principal de urls (dentro de la carpeta del mismo nonmbre del proyecto, donde mismo esta settings.py) debemos agregar una linea a *urls.py* que llame a un archivo de urls de la app:

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('socios.urls')),
]
```

Esto hará que (de momento) el inicio de nuestra web sea lo que definamos en la app socios.

Ahora dentro de la carpeta de nuestra app, en este caso **socios**, se agrega el archivo  *urls.py* que manejará las url de esta app en particular. Agregamos:

```python
urlpatterns = [
    path('', views.socios_form, name='socios_form'),
]
```

El primer parámetro es la url (en este caso vacía porque es el inicio) el segundo la vista y el tercero el nombre de la vista.

#### Crear una vista básica

El archivo **views.py** nos permite cargar (renderear) las vistas que creemos para la app. Agregamos:

```python
def socios_form(request):
    return render(request, 'socios/socios_form.html', {})
```

y ahora debemos crear, dentro de nuestra app, una carpeta **templates** y dentro de esta, en este caso, otra cartpeta **socios** donde podemos ir agregando nuestro html.
Creamos el archivo *socios_form.html* donde podemos copiar nuestro html original de la unidad 1

#### Agregando archivos estáticos comunes (externos a la app)

Debemos colocar al inicio del html, antes de cualquier etiqueta `{% load static %}` y en cada aparición de un archivo que tengamos en `common_static` lo cambiamos por la notación de templates, por ejemplo:

```html
<!-- antes -->
<link rel="stylesheet" href="css/estilos.css">

<!-- ahora -->
<link rel="stylesheet" href="{% static 'css/estilos.css' %}">
```

> En este ejemplo, hasta aqui, el formulario es sólo el html

### 12) "Conectando" formulario a django

#### Ajustando el formulario de la unidad 1

Este es el camino largo y tedioso.
Primero, debemos fijarnos que los campos de *models.py* coincidan con los de nuestro formulario, por ejemplo

```html
<div class="form-group col-md-6">
    <label for="id_campo">Campo</label>
    <input type="text" class="form-control" id="id_campo" name="campo">
</div>
```

Luego el tag `<form>` debe actualizarse:

```html
  <form method="POST" class="post-form">
    {% csrf_token %}
```

Lo importante es que el method sea POST (enviar datos a ser guardados en la BD) y que django pueda generar un token de seguridad.

#### Definiendo el formulario

Dentro del directorio de la app crea un archivo **forms.py** en donde importaremos el modelo:

```python
from django import forms
from .models import Socio

class SocioForm(forms.ModelForm):

    class Meta:
        model = Socio
        fields = ('email', 'rut','nombre_completo', 'fecha_nacimiento', 'telefono', 'ciudad', 'tipo_de_casa')
```

> Atención que cambié el modelo para fecha de nacimiento y teléfono.

#### Actualizando el render de la vista

Ahora le decimos a la vista *views.py* que contiene un formulario:

```python
#agregamos en los imports:
from .forms import SocioForm
#---

def socios_form(request):
    form = SocioForm()
    return render(request, 'socios/socios_form.html', {'form': form})
```

### 13) Manejando la respuesta a la acción de guardar

Al enviar el formulario, el servidor puede detectar que se están enviando dato via POST (http resquest para enviar datos al servidor), por lo que podemos usar esto para manejar las vistas.
Se actualiza el *views.py* para la actual vista del formulario definida en **socios_form** (no olvidar de importar request desde los shortcuts):

```python
def socios_form(request):
    if request.method == "POST":
        form = SocioForm(request.POST)
        if form.is_valid():
            socio = form.save(commit=False)
            socio.save()
            return redirect('socios_res', pk=socio.pk)
    else:
        form = SocioForm()
    return render(request, 'socios/socios_form.html', {'form': form})
```

El **pk** nos devolverá el índice de lo que acabamos de agregar a la BD. Esto podemos usarlo para pasarlo a través de GET y django manejará automáticamente el detalle guardado en la BD

Ahora tenemos que definir lo que ocurre en la vista de respuesta, para eso agregamos:

```python
def socios_respuesta(request, pk):
    socio = get_object_or_404(Socio, pk=pk)
    return render(request, 'socios/socios_respuesta.html', {'socio': socio})
```

Aqui debemos agregar *get_object_or_404* en los shortcuts importados. Esta función nos permite que busque el objeto en la BD según el modelo de Socio, el cual también debemos importar con `from .models import Socio`. Luego, la respuesta de la BD la pasa a la vista en un objeto llamado **socio** que contendrá todos los valores rescatados.

#### URL para la respuesta y paso de variables

Ya tenemos el índice agregado al redireccionamiento, ahora en el archivo *urls.py* de, en este caso, la carpeta socios, agregamos la nueva dirección:

```python
path('socio/<int:pk>/', views.socios_respuesta, name='socios_res'),
```

aqui le decimos que en esa ruta se pasara un valor *int* llamado **pk** que es el que estamos usando para pasar los datos de la BD a la vista.

#### La vista de respuesta

Dentro de, en este caso, *socios/templates/socios* crea otro html, *socios_respuesta.html* se borra `<form>`y se reemplaza por:

```html
<!-- Contacto -->
<div class="contacto col-12">
  <h2>{{ socio.nombre_completo }}</h2>
  <p>Gracias por unírtenos</p>
</div>
```

Esto mostrará el nombre ingresado que obtuvimos a través de *pk* en la BD.

## CRUD

### Forumlarios de django

Aprovechando el potencial de django para ahorrar trabajo a la hora de construir los formularios del BACK END para el administrador del sitio.
En este punto ya esta creado el modelo para, en este caso **rescatados** que manejará la galería de mascotas de la organización. Esto lo identificamos como el *"producto"* principal de la página, pues es la información que la organización quiere destacar para lograr su objetivo, en este caso, dar en adopción mascotas rescatadas.

### 15) CREATE (Agregar)

Debemos hacer similar a cuando "conectamos el formulario, solo que ahi no tenemos una vista front, del usuario final, sino que debemos construir un administrador dedicado al administrador del sitio.

#### Definiendo el formulario CREATE

Dentro del directorio de la app, en este caso *rescatados* crea un archivo **forms.py** en donde importaremos el modelo:

```python
from django import forms
from .models import Rescatado

class RescatadoAdd(forms.ModelForm):

    class Meta:
        model = Rescatado
        fields = ('fotografia', 'nombre','raza_predominante', 'descripcion', 'estado')
```

#### Agregamos la url para CREATE

Primero recuerda agregar el archivo *urls.py* de la app en particular, en este caso rescatados, al archivo principal de urls del proyecto:

```python
path('rescatados/', include('rescatados.urls')),
```

Crea un archivo de *urls.py* dentro de la carpeta de la app con:

```python
from django.urls import path
from . import views

urlpatterns = [
    path('add', views.rescatado_add, name='rescatado_add'),
]
```

#### Agregando la vista para CREATE

En el archivo *views.py* agregamos la lógica de la generación de la vista, considerando que cuando se envía el formulario hay un request de tipo POST y asi podemos gestionar la respuesta al envío.
En este caso en que se sube un archivo, además de *request.POST* debemos usar *request.FILES*

```python
from django.shortcuts import render, redirect
from .forms import RescatadoAdd

# Create your views here.

def rescatado_add(request):
    if request.method == "POST":
        form = RescatadoAdd(request.POST, request.FILES)
        if form.is_valid():
            rescatado.save()
            return redirect('rescatado_detail', pk=rescatado.pk)
    else:
        form = RescatadoAdd()
    return render(request, 'rescatados/rescatado_add.html', {'form': form})
```

#### Creamos el template para CREATE

Dentro de la carpeta de la app, en este caso *rescatados*, agregamos la carpeta **templates** y dentro de esta la carpeta **rescatados**. En esta ubicación creamos el archvio *rescatado_add.html* que definimos en el view y que contendrá el nuevo formulario.
Para esto utiliza un encabezado y un footer acorde al front, pero sin la navegación de este, pues estamos haciendo otro sitio.

Para usar los formularios automáticos de django, debemos seguir las reglas:

- Debes contener el formulario dentro de un `<form>` que tenga el metodo POST
- Para generar los campos de formulario se usa *{{ form.as_p}}*
- Desbes tener un boton de tipo *submit* para guardar.
- Por seguridad, debes tener {% csrf_token %} justo después de la apertura del formulario
- Si el formulario tiene campos para subir archivos, el form debe tener el atributo `enctype="multipart/form-data"`

```html
    <form method="POST" enctype="multipart/form-data">
    {% csrf_token %}
        {{ form.as_p }}
        <button type="submit">Agregar</button>
    </form>
```

#### Vista de respuesta a CREATE

Debemos redireccionarlo a la lista o al detalle de nuestro modelo, abordado a continuación.

### 15) READ (leer)

Primero se aborda el listado

#### Agregamos las urls para listar resultados

Como normalmente es la primera página de un administrador, lo dejaremos como página indice del módulo, agregando en *urls.py* de la carpeta del módulo `path('add', views.rescatado_add, name='rescatado_add'),`

#### Agregamos la vista para listar

En *views.py* de la carpeta del módulo, agregamos 'rescatado_list' que definimos anteriormente, sin olvidar agregar el import del modelo, pues será utilizado en la vista

```python
from .models import Rescatado

def rescatado_list(request):
    return render(request, 'rescatados/rescatado_list.html', {})
```

#### Creamos el template para el listado

Creamos el archivo *rescatado_list.html* dentro de la carpeta templates/rescatados, en este caso.

---
Ahora el detalle

#### Agregamos el link al detalle

En el template del listado, debe ser algo similar a

```html
<h5 class="card-title"><a href="{% url 'rescatado_detail' pk=rescatado.pk %}">{{ rescatado.nombre }}</a></h5>
```

#### Agregamos la url para el detalle

Como normalmente es la primera página de un administrador, lo dejaremos como página indice del módulo, agregando en *urls.py* de la carpeta del módulo `path('<int:pk>/', views.rescatado_detail, name='rescatado_detail'),`

#### Agregamos la vista para detalle

En *views.py* de la carpeta del módulo, agregamos 'rescatado_detail'

```python
def rescatado_detail(request, pk):
    rescatado = get_object_or_404(Rescatado, pk=pk)
    return render(request, 'blrescatadosog/rescatado_detail.html', {'rescatado': rescatado})
```

#### Creamos el template para el detalle

Creamos el archivo *rescatado_detail.html* dentro de la carpeta templates/rescatados, en este caso.

#### Redireccionar el resultado de crear

Debemos comprobar que se utilice correctamente el nombre de la vista, en este caso 'rescatado_detail'

### 16) EDIT (editar)

#### Agregamos el link para editar

Actualizamos el template de listados:

```html
<p class="card-text"><a class="btn btn-default" href="{% url 'rescatado_edit' pk=rescatado.pk %}">Editar</a></p>
```

#### Agregamos la url para el editar

Agregamos a *urls.py* del módulo `path('<int:pk>/edit/', views.post_edit, name='rescatado_edit'),`

#### Agregamos la vista para editar

Aqui lo que le decimos a django es que utilice el mismo formulario que usamos para agregar pero con los datos pre cargados, instanciando, en este caso, un objeto del modelo **rescatado**

```python
def rescatado_edit(request, pk):
    rescatado = get_object_or_404(Rescatado, pk=pk)
    if request.method == "POST":
        form = RescatadoAdd(request.POST, instance=rescatado)
        if form.is_valid():
            rescatado = form.save(commit=False)
            rescatado.save()
            return redirect('rescatado_list')
    else:
        form = RescatadoAdd(instance=rescatado)
    return render(request, 'rescatados/rescatado_edit.html', {'form': form})
```

#### Creamos el template para el editar

Creamos el archivo *rescatado_edit.html* dentro de la carpeta templates/rescatados, en este caso. También podemos "reciclar" la vista del mismo formulario que usamos para agregar, siempre que en *views.py* le indiquemos ese template.

### 16) DELETE (eliminar)

Esto podemos hacerlo solo con la url, sin necesidad vista. Como recomendación se sugiere agregar, via javascript o modal, un aviso al usuario para asegurar la acción.

#### Agregamos el link para eliminar

Actualizamos el template de listados:

```html
<p class="card-text"><a class="btn btn-danger" href="{% url 'rescatado_delete' pk=rescatado.pk %}">Eliminar</a></p>
```

#### Agregamos la url para el eliminar

Agregamos a *urls.py* del módulo `path('<int:pk>/edit/', views.post_edit, name='rescatado_edit'),`

#### Agregamos la vista para eliminar

Aqui lo que le decimos a django que busque la entreda en la bd y la elimine

```python
def rescatado_delete(request, pk):  
    rescatado = Rescatado.objects.get(pk=pk)  
    rescatado.delete()  
    return redirect('rescatado_list')
```

### 17) El error 'Class has no objets member

Este es un error que salta por el lint de código pero que no genera problemas de funcionamiento.
Si bien el camino corto es configurar el lint, en términos de estándares de programación sería agregar **objects** al modelo, lo cual se hace simplemente agregando `objects = models.Manager()`al final de la clase, dentro del archivo *models.py*
<https://stackoverflow.com/questions/45135263/class-has-no-objects-member>

### Una guía corta, exacta y correcta para el CRUD

En esta web encontrarán varios tutoriales de django, el del crud es bien bueno
<https://www.javatpoint.com/django-crud-example>

### 18) Manejo de links dentro del sitio

La forma mas útil es usar los nombre de las urls: '{% url 'nombre_url' %}'

## PRUEBAS UNITARIAS

Tenemos por un lado **unittest** que es un módulo de python, pero se recomienda usar **pytest** para que se haga mas fácil la escritura de pruebas funcionales. Para instalar `pip install pytest pytest-django`, que automáticamente nos instalará pytest. Exiten muchas mas herramientas para complementar el QA en django, como coverage para analizar la cobertura, tox para automatizar la ejecución, otras herramientas para testlink, etc.

### 18) Configuración de pytest

Primero tenemos que indicarle a pytest que configuración de django debe usar. Para esto, en la raiz del proyecto creamos el archivo **pytest.ini** que contenga:

```ini
[pytest]
DJANGO_SETTINGS_MODULE = misperris2020.settings
python_files = tests.py test_*.py *_tests.py
```

Listo, ta se puede correr los test usando `pytest`en la consola. Nos dirá que no encontró ninguno.

### 19) Archivo para pruebas

Dentro del módulo (app) que queremos probar, ya tenemos el archivo *tests.py* que es el lugar para escribir nuestro código de pruebas por defecto.

También se puede usar una estructura de archivos ordenada, donde, dentro de una carpeta test, iremos guardano los archivos segun sobre que ámbito realizan el test, por ejemplo:

```tree
tests
    test_models.py
    test_urls.py
    test_views.py
```

En este ejemplo, lo haremos dentro del módulo **rescatados**

### 20) Correr los test

Sólo se debe usar, via consola `pytest` desde la raiz del proyecto.

## REST-FRAMEWORK

### 21) Instalar plugin

Debemos ejecutar en consola `pip install djangorestframework` y agregar en *settings.py* en INSTALLED_APPS:

```python
INSTALLED_APPS = [
    #aqui van todas las apps creadas y plugins instaladas
    'rest_framework',
]
```

En el mismo archivo de *settings.py* al final, agregamos la configuración del plugin:

```python
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10
}
```

### 22) Agregar la app para desarrollar la api

Hacemos lo mismo que en el paso 4, solo que ahora se llamara **api** o similar. Desde ahi manejaremos todos los servicios.
Debemos crear el archivo *urls.py* dentro de la carpeta *api* y desde el archivo principal de **urls** agregarlo:

```python
urlpatterns = [
    #Aqui las url que ya tenemos
    path('api/', include('rest_framework.urls'))
]
```

### 23) Armar el ruteador

En el archivo *urls.py* de nuestra nueva **api** debemos agregar el código:

```python
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'rescatados', views.RescatadosListaJson)
router.register(r'socios', views.SociosListaJson)

urlpatterns = [
    path('', include(router.urls)),
]
```

En este ejemplo armará servicios para *rescatados* y *socios* por lo tanto en el archivo de las vistas los tenemos que tener definidos

### 24) Armar la vista

El archivo nos debería quedar:

```python
from rest_framework import viewsets, permissions
from .serializers import RescatadosListaSerializer, SociosListaSerializer

#Modelos que necesito llamar
from rescatados.models import Rescatado
from socios.models import Socio

# Create your views here.

class RescatadosListaJson(viewsets.ModelViewSet):
    """
    API endpoint que lista los animales rescatados.
    """

    queryset = Rescatado.objects.all().order_by('nombre')
    serializer_class = RescatadosListaSerializer
    #permission_classes = [permissions.IsAuthenticated]

class SociosListaJson(viewsets.ModelViewSet):
    """
    API endpoint que lista los socios de la organización.
    """
    queryset = Socio.objects.all().order_by('email')
    serializer_class = SociosListaSerializer
    #permisson_classes = [permissions.IsAuthenticated]
```

Al comentar la linea de autenticación se puede ver el endpoint sin necesidad de estar logueado, lo cual es un peligro de seguridad que no debe pasar a producción. Normalmente se maneja via token, oauth y similares.

En este código vemos que en vez de cargar un template carga un serializer, que debemos crear

### 25) Creando el serializador

Cuando estamos haciendo una API REST no necesitamos enviar al navegador el documento html por lo que en vez de usar los templates de django, el plugin permite cargar serializadores que nos indican cómo debe armarse la respuesta en json

Debemos crear un archivo *serializers.py* dentro de nuestra **api**

```python
from rest_framework import serializers
#Modelos que se usan
from rescatados.models import Rescatado
from socios.models import Socio

class RescatadosListaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rescatado
        fields = ['fotografia', 'nombre', 'estado']

class SociosListaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Socio
        fields = '__all__'
```

Aqui podemos ver que se puede utilizar solo los campos seleccionados o indicarle que traiga todos los campos.

### 26) Probar la api

Levantamos el servior y navegamos a la url que utilizamos para la app *api*, por ejemplo <http://127.0.0.1:8000/api/>

### 27) Consumir la API desde el front: CORS-HEADERS

Para que podamos usar esta api desde otra web o aplicación, debemos instalar el plugin: `pip install django-cors-headers`
Aqui hay que tener cuidado porque estamos abriendo nuestra api a cualquiera, por lo que es un riesgo de seguridad. Se debe combinar OAuth, token y los permisos de *settings.py* a sitios específicos para no dejar nuestro sistema expuesto.

En el archivo *settings.py* abrimos los endpoints (esto es con fines educativos, deben tener cuidado con la seguridad cuando son proyectos)

```python
# Cambiamos ALLOWED HOST por un comodin que nos permite cualquier conexión y permitimos los origenes externos
ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = Ture

# La agregamos a las apps, que ya debería estar similar:
INSTALLED_APPS = [
    #etc
    'rest_framework',
    'api.apps.ApiConfig',
    'corsheaders',
]

# Agregamos el MIDDLEWARE
MIDDLEWARE = [
    #otros aqui
    'corsheaders.middleware.CorsMiddLeware',
]
```

El tutorial completo, includo configuración segura : <https://dzone.com/articles/how-to-fix-django-cors-error>

## Mezclar objetos de dos app distintas dentro de django

Como ejemplo, en la página home que corresponde al formulario de socios, se agregará una galería de imágenes de rescatados.

### 28) Traer los datos del modelo a la vista

En el archivo *views.py* de, en ese ejemplo, **socios** agregamos la importación del modelo de **rescatados**

```python
from rescatados.models import Rescatado
```

En este caso, lo queremos usar en el mismo template que arma el formulario de socios, por lo que debemos cambiar las lineas del **else** para el armado de la vista *socios_form*

```python
    else:
        form = SocioForm()
        rescatados = Rescatado.objects.all() # Traigo los datos de rescatados
    return render(request, 'socios/socios_form.html', {'form': form, 'rescatados': rescatados}) #Envio el objeto form y el objeto rescatados al template
```

### 29) Agregar la galería rescatados al template

Aqui podemos "reciclar" la galería que hay para la administración de rescatados que hay en */rescatados/templates/rescatados/rescatado_list.html* y colocarla en el template que usamos en el home, */socios/templates/socios/socios_form.html* quitando los elementos html asociados a la edición:

```html
<!-- Listado -->
<div class="admin galeria">
  <h3>Rescatados</h3>  
  <div class="card-columns">  
      {% for rescatado in rescatados %}
      <div class="card text-white w-300 border-0">
          <img src="{{ rescatado.fotografia.url }}" class="card-img opaco" alt="{{ rescatado.nombre }}">
          <div class="card-img-overlay">
            <h5 class="card-title">{{ rescatado.nombre }}</h5>
            <p class="card-text">{{ rescatado.estado }}</p>
          </div>
        </div>
      {% endfor %}
  </div>
</div>
```

## PWA - Progresive web aplication en Django

La PWA nos peremitirá ampliar las opciones para utilizar elementos presenten en un dispositivo móvil como si fuera una aplicación. Principalmente la opción de cachear archivos y permitir el trabajo offline.
Para esto se usará el plugin: <https://github.com/silviolleite/django-pwa>

### 30) Instalar y configurar django-pwa

Se debe ejecutar 'pip instal django-pwa' y no olvidar actualizar el archivo **requirements.txt

En el archivo *settings.py* se debe agergar 'pwa' a la lista de apps instaladas

```python
INSTALLED_APPS = [
    #otras apps
    'pwa'
]
```

Y agregar la localización del server worker (en este caso, la raiz del proyecto) y crear el archivo con el mismo nombre y localización que indiquemos

```python
PWA_SERVICE_WORKER_PATH = os.path.join(BASE_DIR, 'serviceworker.js')
```

Nota: si vamos a usar 'os.path.join' debemos agregar en los import:

```python
import os
```

Si queremos desactivar los mensajes de consola para PWA (cuando pasemos el proyecto a producción):

```python
PWA_APP_DEBUG_MODE = False #o True si queremos que se muestre
```

### 31) Configrar la url para la pwa

En el archivo principal *urls.py* agregamos a la lista de **urlpatterns**:

```python
    path('', include('pwa.urls')),
    #podemos usar url en vez de path pero se debe agregar a los import
```

### 32) Agregar etiquetas al template principal

Lo ideal es hacer esto en el template *base.html* o el que maneje el head de todo el sitio. En el caso de este ejemplo, a falta de este archivo, lo haremos en el template del home, *socios_form.html*

```html
<html lang="es">
  {% load pwa %}
  <head>
    {% progressive_web_app_meta %}
```

### 33) Configurar manifiest

Este plugin permite configurar parámetros del manifiest a travez del archivo *settings.py*:

```python
#Configuracion del manifiest PWA
PWA_APP_NAME = 'Mis perrris' #Nombre
PWA_APP_DESCRIPTION = "App de ejemplo para la clase DWY4101" # Descripción
PWA_APP_THEME_COLOR = '#85D4B7'
PWA_APP_BACKGROUND_COLOR = '#ffffff'
PWA_APP_DISPLAY = 'standalone'
PWA_APP_SCOPE = '/'
PWA_APP_ORIENTATION = 'any'
PWA_APP_START_URL = '/'
PWA_APP_STATUS_BAR_COLOR = 'default'
PWA_APP_ICONS = [ # Icono para intalación como app
    {
        'src': '/static/imagenes/icono160.png',
        'sizes': '160x160'
    }
]
PWA_APP_ICONS_APPLE = [
    {
        'src': '/static/imagenes/icono160apple.png',
        'sizes': '160x160'
    }
]
PWA_APP_SPLASH_SCREEN = [ # Imagen Splash cuando se levanta como app
    {
        'src': '/static/imagenes/splash640x1136.png',
        'media': '(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)'
    }
]
PWA_APP_DIR = 'ltr'
PWA_APP_LANG = 'es-cl'
```

Se aconseja configurar al menos el nombre, la descripción, el icono principal y la imagen splash. Se deben crear las imágenes y guardarlas con el mismo nombre y ruta configuradas

### 34) Programar el server worker

Podemos utilizar el mismo código de ejemplo del plugin, actualizándolo a nuestras necesidades en el archivo *serverworker.js* que creamos:

```js
// Nombre que tendrá la cache del sitio. En este caso va versionado por fecha y hora, lo cual es opcional
var staticCacheName = "mis-perris-v" + new Date().getTime();

// La ruta de los archivos que quiero cachear
var filesToCache = [
    '/',
    '/offline',
    '/static/css/estilos.css',
    '/static/imagenes/logo.png',
    '/static/librerias/fontawesome/all.min.css',
];

// Cache al instalar (se mantiene el código original)
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Limpiar cache al activar (se mantiene código original)
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("mis-perris-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Servicio desde el cache (código actualizado para que cachee todos los elementos con clone)
self.addEventListener("fetch", event => {
    event.respondWith(
        fetch(event.request).then((result)=>{
            return caches.open(staticCacheName).then(function(c) {
                c.put(event.request.url, result.clone())
                return result;
            })
        }).catch(function(e){
            return caches.match(event.request)
        })
    )
});
```

## Agregar boton pra instalar como app

Referencias:

<https://web.dev/customize-install/>
<https://web.dev/install-criteria/>
<https://web.dev/codelab-make-installable/>
<https://web.dev/promote-install/>

<https://github.com/JoseJPR/tutorial-pwa-add-home-screen> (de aqui se tomó el código)

### 35) Agregar html en el template

En este caso **socios/templates/socios/socios_form.html** que es el template que se ve en la página inicial

```html
<!--Boton para PWA-->
    <div id="prompt" class="prompt hide bg-light border-top">
      <div class="font-weight-bold">Agregar Aplicación</div>
      <small>Esta aplicación web puede ser instalada</small>
      <div class="text-right">
        <button id="buttonCancel" type="button" class="font-weight-bold text-muted btn-sm btn btn-link">CANCELAR</button>
        <button id="buttonAdd" type="button" class="font-weight-bold text-primary btn-sm btn btn-link">ACEPTAR</button>
      </div>
    </div>
```

### 36) Crear archivo JS con la lógica

En este caso, dentro de *common_static*, donde tengo mis archivos estaticos comunes a todo el sitio, creo al carpeta **js** y el archivo **btnpwa.js** dentro de esta carpeta y agrego el código (pueden copiar todo el archivo tal cual <https://gitlab.com/alaravena/misperris2020/-/blob/master/common_static/js/btnpwa.js>)

### 37) Crear archivo css con visual de su sitio

En este caso, ya tenemos una carpeta **css** dentro de *common_static*, por lo que solo queda crear el archivo **btnpwa.css** dentro de ésta. El código es similar a <https://gitlab.com/alaravena/misperris2020/-/blob/master/common_static/css/btnpwa.css> pero se recomienda cambiar el color de fondo y comprobar que la imagen que se quiera usar esté en la ruta adecuada

### 38) Llamar los archivos css y js

Ahora solo queda llamar correctamente los archivos dentro del template, en este caso dentro de **socios/templates/socios/socios_form.html**

```html
<!-- En el head, despues de el CSS principal -->
<link rel="stylesheet" href="{% static 'css/btnpwa.css' %}">

<!-- Al final, despues de las otras librerías JS-->
<script src="{% static 'js/btnpwa.js' %}" ></script>
```

Y recordar que simpre que se cree o altere un archivo estático, se debe correr en la consola `python manage.py collectstatic`
