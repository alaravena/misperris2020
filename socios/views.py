from django.shortcuts import render, redirect, get_object_or_404
from .forms import SocioForm
from .models import Socio
from rescatados.models import Rescatado


# Create your views here.
def socios_form(request):
    if request.method == "POST":
        form = SocioForm(request.POST)
        if form.is_valid():
            socio = form.save(commit=False)
            socio.save()
            return redirect('socios_res', pk=socio.pk)
    else:
        form = SocioForm()
        rescatados = Rescatado.objects.all()
    return render(request, 'socios/socios_form.html', {'form': form, 'rescatados': rescatados})

def socios_respuesta(request, pk):
    socio = get_object_or_404(Socio, pk=pk)
    return render(request, 'socios/socios_respuesta.html', {'socio': socio})
