from django.db import models
from django.utils import timezone

#Opciones selector Tipo de casa
TIPO_CASA_CHOICES = (
    ('casa-patio-l','Casa con patio grande'),
    ('casa-patio-s', 'Casa con patio pequeño'),
    ('casa','Casa sin patio'),
    ('depto','Departamento'),
)

# Create your models here.
class Socio(models.Model):
    email = models.EmailField(max_length=200)
    rut = models.CharField(max_length=10)
    nombre_completo = models.CharField(max_length=200, null=False)
    fecha_nacimiento = models.DateField(default='1980-12-05')
    telefono = models.CharField(max_length=15, default='+562454554226')
    ciudad = models.CharField(max_length=200)
    tipo_de_casa = models.CharField(max_length=25, choices=TIPO_CASA_CHOICES, default='casa-patio-l')

    def __str__(self):
        return self.nombre_completo