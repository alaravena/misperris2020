from django import forms
from .models import Socio

class SocioForm(forms.ModelForm):

    class Meta:
        model = Socio
        fields = ('email', 'rut','nombre_completo', 'fecha_nacimiento', 'telefono', 'ciudad', 'tipo_de_casa')