from django.apps import AppConfig


class DestacadosConfig(AppConfig):
    name = 'destacados'
