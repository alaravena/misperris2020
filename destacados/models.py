from django.db import models
from django.utils import timezone

# Create your models here.
class Destacado(models.Model):
    imagen_destacada = models.ImageField(null=False,upload_to = 'img-destacados')
    titutlo_destacado = models.CharField(max_length=200)

    def __str__(self):
        return self.titutlo_destacado
